var DIR_LIST_FILEPATH = 'C:\\S3 Sync\\dirs.txt';
var SEPERATOR = '___|||___';


// creates a dir list entry for adding to the dir list
// returns null if the given syncPair is invalid
function createDirListEntry(syncPair) {
    if (!syncPair.dir || syncPair.dir.length === 0 ||
        !syncPair.s3BucketPath || syncPair.s3BucketPath.length === 0) {
        return null;
    }
    
    return syncPair.dir + SEPERATOR + syncPair.s3BucketPath;
}


// parses a dir list entry
// returns a sync pair or null if the given dirListEntry is invalid
function parseDirListEntry(dirListEntry) {
    if (!dirListEntry || dirListEntry.length === 0) {
        return null;
    }
    
    var index = dirListEntry.indexOf(SEPERATOR);
    if (index === -1) {
        return null;
    }
    
    var dir = dirListEntry.substr(0, index);
    var s3BucketPath = dirListEntry.substr(index + SEPERATOR.length);
    
    if (dir.length === 0 || s3BucketPath.length === 0) {
        return null;
    }
    
    return {
        dir: dir,
        s3BucketPath: s3BucketPath
    };
}


// adds a dir list entry to the dir list
function addToDirList(dirListEntry) {
    // write the dir to the dir list file
    var fso = new ActiveXObject('Scripting.FileSystemObject');
    var file = fso.GetFile(DIR_LIST_FILEPATH);
    
    fs = file.OpenAsTextStream(8);
    fs.WriteLine(dirListEntry);
    fs.Close();
}


// reads all entries in the dir list
// returns an array of sync pairs
function readDirList() {
    var syncPairs = [];
    
    // read dir list file
    var fso = new ActiveXObject('Scripting.FileSystemObject');
    var file = fso.GetFile(DIR_LIST_FILEPATH);
    
    var fs = file.OpenAsTextStream(1);
    
    var lineNum = 0;
    while (!fs.AtEndOfStream) {
        var line = fs.ReadLine();
        ++lineNum;
        
        // skip empty lines
        if (line.length === 0) {
            continue;
        }
        
        // parse entry
        var dirListEntry = line;
        var syncPair = parseDirListEntry(dirListEntry);
        
        if (syncPair === null) {
            throw new Error('Invalid dir list entry on line ' + lineNum + '.');
        }
        
        syncPairs.push(syncPair);
    }
    
    fs.Close();
    return syncPairs;
}


// syncs the given sync pair with S3
function syncDir(syncPair, download) {
    var dir = syncPair.dir;
    var s3BucketPath = syncPair.s3BucketPath;
    
    // remove trailing slashes
    dir = dir.substring(0, dir.length - 1);
    s3BucketPath = s3BucketPath.substring(0, s3BucketPath.length - 1);
    
    var localParam = '"' + escapeParam(dir) + '"';
    var remoteParam = '"' + escapeParam('s3://' + s3BucketPath) + '"';
    
    var params;
    if (download) {
        params = remoteParam + ' ' + localParam;
    }
    else {
        params = localParam + ' ' + remoteParam;
    }
    
    var shell = WScript.CreateObject('WScript.Shell');
    var exec = shell.Exec('aws s3 sync ' + params);
    
    var stdout = '';
    var stderr = '';
    
    do {
        while (!exec.StdOut.AtEndOfStream) {
          stdout += exec.StdOut.ReadLine();
        }
        while (!exec.StdErr.AtEndOfStream) {
          stderr += exec.StdErr.ReadLine();
        }
        
        WScript.Sleep(100);
    }
    while (exec.Status === 0);
    
    var exitCode = exec.ExitCode;
    
    return {
        exitCode: exitCode,
        stdout  : stdout,
        stderr  : stderr
    };
}

function escapeParam(paramStr) {
	return paramStr.replace(new RegExp('\\(\\)%\\!\\^"<>\\&\\|', 'g'), '^$1');
}



// ===========================================
// ===========================================
// ===========================================



String.prototype.trim = String.prototype.trim || function () {
  return this.replace(/^\s+|\s+$/gm,'');
}
